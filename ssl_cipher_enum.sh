#!/bin/bash

# Frank Brodbeck <fab@clacks.xyz>

SERVER=$1
ME=$(basename $0)

_usage() {
        cat <<EOF >&2
usage: $ME <server>
  provide *exactly* one server to enumerate
EOF
        exit 2
}

_chk_cipher() {
    local _server=$1
    local _prot=$2
    openssl ciphers -v | awk -v prot=$_prot '{ if ( $2==prot ) print $1}' | while read cipher; do
            printf "    Testing %s ... " "$cipher"
            openssl s_client -connect $_server -cipher $cipher 2>&1 </dev/null | awk '
	    BEGIN { errc=-1 ; ncipher=-1 }
	    /^[0-9]+.*error/{ errc=split($0,err,":") }
            /    Cipher.*:/{ ncipher=$NF }
	   END {
		if (errc != -1)
			printf "error: %s\n",err[6]
		else if (ncipher == -1)
			print "no"
		else
			printf "yes\n",ncipher
           }'
	   sleep 1
    done
}

[ -z $SERVER ] && _usage

openssl ciphers -v | awk '{ prot[$2]=1 } END{ for (p in prot) printf("%s\n",p)}' | while read _prot; do
	printf "Using protocol %s\n" "$_prot"
	_chk_cipher $SERVER $_prot
done
