This is a small script for SSL cipher enumeration based on shell and openssl. Helpful for restricted environments where advanced tools are not available.

# Example usage

```
# ./ssl_cipher_enum.sh host:443
Using protocol SSLv3
    Testing ECDHE-RSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing ECDHE-ECDSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing SRP-DSS-AES-256-CBC-SHA ... error: no ciphers available
    Testing SRP-RSA-AES-256-CBC-SHA ... error: no ciphers available
    Testing SRP-AES-256-CBC-SHA ... error: no ciphers available
    Testing DHE-RSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES256-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES256-SHA ... error: sslv3 alert handshake failure
    Testing DHE-RSA-CAMELLIA256-SHA ... error: sslv3 alert handshake failure
    Testing DHE-DSS-CAMELLIA256-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-CAMELLIA256-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-CAMELLIA256-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES256-SHA ... error: sslv3 alert handshake failure
    Testing AES256-SHA ... error: sslv3 alert handshake failure
    Testing CAMELLIA256-SHA ... error: sslv3 alert handshake failure
    Testing PSK-AES256-CBC-SHA ... error: no ciphers available
    Testing ECDHE-RSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing ECDHE-ECDSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing SRP-DSS-AES-128-CBC-SHA ... error: no ciphers available
    Testing SRP-RSA-AES-128-CBC-SHA ... error: no ciphers available
    Testing SRP-AES-128-CBC-SHA ... error: no ciphers available
    Testing DHE-RSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES128-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES128-SHA ... error: sslv3 alert handshake failure
    Testing DHE-RSA-SEED-SHA ... error: sslv3 alert handshake failure
    Testing DHE-DSS-SEED-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-SEED-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-SEED-SHA ... error: sslv3 alert handshake failure
    Testing DHE-RSA-CAMELLIA128-SHA ... error: sslv3 alert handshake failure
    Testing DHE-DSS-CAMELLIA128-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-CAMELLIA128-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-CAMELLIA128-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES128-SHA ... error: sslv3 alert handshake failure
    Testing AES128-SHA ... error: sslv3 alert handshake failure
    Testing SEED-SHA ... error: sslv3 alert handshake failure
    Testing CAMELLIA128-SHA ... error: sslv3 alert handshake failure
    Testing IDEA-CBC-SHA ... error: sslv3 alert handshake failure
    Testing PSK-AES128-CBC-SHA ... error: no ciphers available
    Testing ECDHE-RSA-RC4-SHA ... error: sslv3 alert handshake failure
    Testing ECDHE-ECDSA-RC4-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-RC4-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-RC4-SHA ... error: sslv3 alert handshake failure
    Testing RC4-SHA ... error: sslv3 alert handshake failure
    Testing RC4-MD5 ... error: sslv3 alert handshake failure
    Testing PSK-RC4-SHA ... error: no ciphers available
    Testing ECDHE-RSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing ECDHE-ECDSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing SRP-DSS-3DES-EDE-CBC-SHA ... error: no ciphers available
    Testing SRP-RSA-3DES-EDE-CBC-SHA ... error: no ciphers available
    Testing SRP-3DES-EDE-CBC-SHA ... error: no ciphers available
    Testing EDH-RSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing EDH-DSS-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing DH-RSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing DH-DSS-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing DES-CBC3-SHA ... error: sslv3 alert handshake failure
    Testing PSK-3DES-EDE-CBC-SHA ... error: no ciphers available
Using protocol TLSv1.2
    Testing ECDHE-RSA-AES256-GCM-SHA384 ... yes
    Testing ECDHE-ECDSA-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing ECDHE-RSA-AES256-SHA384 ... yes
    Testing ECDHE-ECDSA-AES256-SHA384 ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing DHE-RSA-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing DHE-RSA-AES256-SHA256 ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES256-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES256-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES256-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES256-SHA384 ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES256-SHA384 ... error: sslv3 alert handshake failure
    Testing AES256-GCM-SHA384 ... error: sslv3 alert handshake failure
    Testing AES256-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDHE-RSA-AES128-GCM-SHA256 ... yes
    Testing ECDHE-ECDSA-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDHE-RSA-AES128-SHA256 ... yes
    Testing ECDHE-ECDSA-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing DHE-RSA-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing DHE-RSA-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing DHE-DSS-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-RSA-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing DH-DSS-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDH-RSA-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing ECDH-ECDSA-AES128-SHA256 ... error: sslv3 alert handshake failure
    Testing AES128-GCM-SHA256 ... error: sslv3 alert handshake failure
    Testing AES128-SHA256 ... error: sslv3 alert handshake failure
#
```
